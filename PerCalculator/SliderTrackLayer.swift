import UIKit
import QuartzCore

class SliderTrackLayer: CALayer {
    
    var track = CAShapeLayer()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    weak var slider: Slider?
    
    override func drawInContext(ctx: CGContext) {
        if let slider = slider {
            setUpLayer()
        }

    }
    // && appDelegate.appWasInBackground == false
    func setUpLayer() {
        if appDelegate.calcScreen == false {
            //let lowerValuePosition = CGFloat(0.0)
            let width = CGFloat(slider!.positionForValue(slider!.lowerValue))
            
            let trackHeight = bounds.height/2
            
            track.frame = CGRect(x: 0.0, y: (bounds.height-trackHeight)/2, width: width, height: bounds.height/2)
            track.backgroundColor = Colors().red
            
            addSublayer(track)
        }
    }
    
}