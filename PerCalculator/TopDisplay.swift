import UIKit
import QuartzCore

class TopDisplay: CALayer {
    
    //override var frame: CGRect
    let tm = CATextLayer() // Text main
    let tl = CATextLayer() // Test label
    //var number2display:Double = 100.00
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    weak var displays: Displays?
    
    var clicked: Bool = false {
        didSet {
            //setNeedsDisplay()
            //println("TopDIsplay clicked")
        }
    }
    
    override func drawInContext(ctx: CGContext) {
        if let displays = displays {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        if appDelegate.appWasInBackground == false {
            //println("=== TopDisplay setUpLayer ===")
            
            let topMargin = bounds.height * 0.35
            let displayHeight = frame.height * 0.25
            tm.frame = CGRect(x:0,y:topMargin,width:bounds.width,height:displayHeight+10)
            tm.foregroundColor = Colors().red
            tm.fontSize = frame.height * 0.25
            tm.font = CTFontCreateWithName("AvenirNextCondensed-Regular", tm.fontSize, nil)
            tm.alignmentMode = kCAAlignmentCenter
            tm.contentsScale = UIScreen.mainScreen().scale
            
            //        let attributes = [
            //            NSFontAttributeName : titleFont,
            //            kCTKernAttributeName :
            //        ]
            
            tm.string = "" //NSAttributedString(string: "", attributes: attributes)
            
            addSublayer(tm)
            
            tl.frame = CGRect(x:0,y:tm.frame.origin.y+tm.bounds.height,width:bounds.width,height:bounds.height) //tm.frame.height/2+topMargin/2
            tl.foregroundColor = Colors().darkBlue
            tl.opacity = 0.4
            tl.fontSize = frame.height * 0.04
            tl.font = CTFontCreateWithName("AvenirNext-DemiBold", tm.fontSize, nil)
            tl.alignmentMode = kCAAlignmentCenter
            tl.contentsScale = UIScreen.mainScreen().scale
            tl.string = "original amount"
            
            addSublayer(tl)
        }
    }
    
}