//
//  delKey.swift
//  Perculator
//
//  Created by Tomas Bulva on 8/6/15.
//  Copyright (c) 2015 cchangeinc. All rights reserved.
//
import UIKit
import QuartzCore
import Foundation

class DelKey: CALayer {
    
    weak var calculator: Calculator?
    var theKeyOuter = CAShapeLayer()
    var theKeyInner = CAShapeLayer()
    
    override func drawInContext(ctx: CGContext) {
        if let calculator = calculator {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        
        //println("DelKey setUpLayer")
        
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(18.56, -0.09))
        bezierPath.addLineToPoint(CGPointMake(7.65, -0.09))
        bezierPath.addCurveToPoint(CGPointMake(4.71, 1.59), controlPoint1: CGPointMake(6.45, -0.09), controlPoint2: CGPointMake(5.32, 0.55))
        bezierPath.addLineToPoint(CGPointMake(0.76, 7.62))
        bezierPath.addLineToPoint(CGPointMake(0.73, 7.66))
        bezierPath.addLineToPoint(CGPointMake(0.71, 7.7))
        bezierPath.addCurveToPoint(CGPointMake(0.71, 11.1), controlPoint1: CGPointMake(0.11, 8.75), controlPoint2: CGPointMake(0.11, 10.05))
        bezierPath.addLineToPoint(CGPointMake(0.73, 11.14))
        bezierPath.addLineToPoint(CGPointMake(0.76, 11.18))
        bezierPath.addLineToPoint(CGPointMake(4.71, 17.21))
        bezierPath.addCurveToPoint(CGPointMake(7.66, 18.89), controlPoint1: CGPointMake(5.32, 18.25), controlPoint2: CGPointMake(6.45, 18.89))
        bezierPath.addLineToPoint(CGPointMake(18.56, 18.89))
        bezierPath.addCurveToPoint(CGPointMake(21.98, 15.47), controlPoint1: CGPointMake(20.44, 18.89), controlPoint2: CGPointMake(21.98, 17.36))
        bezierPath.addLineToPoint(CGPointMake(21.98, 3.33))
        bezierPath.addCurveToPoint(CGPointMake(18.56, -0.09), controlPoint1: CGPointMake(21.98, 1.45), controlPoint2: CGPointMake(20.44, -0.09))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(18.56, 1.41))
        bezierPath.addCurveToPoint(CGPointMake(20.48, 3.33), controlPoint1: CGPointMake(19.62, 1.41), controlPoint2: CGPointMake(20.48, 2.27))
        bezierPath.addLineToPoint(CGPointMake(20.48, 15.47))
        bezierPath.addCurveToPoint(CGPointMake(18.56, 17.39), controlPoint1: CGPointMake(20.48, 16.53), controlPoint2: CGPointMake(19.62, 17.39))
        bezierPath.addLineToPoint(CGPointMake(7.66, 17.39))
        bezierPath.addCurveToPoint(CGPointMake(5.98, 16.42), controlPoint1: CGPointMake(6.96, 17.39), controlPoint2: CGPointMake(6.33, 17.02))
        bezierPath.addLineToPoint(CGPointMake(2.01, 10.36))
        bezierPath.addCurveToPoint(CGPointMake(2.01, 8.44), controlPoint1: CGPointMake(1.67, 9.77), controlPoint2: CGPointMake(1.67, 9.04))
        bezierPath.addLineToPoint(CGPointMake(5.98, 2.38))
        bezierPath.addCurveToPoint(CGPointMake(7.65, 1.41), controlPoint1: CGPointMake(6.33, 1.78), controlPoint2: CGPointMake(6.96, 1.41))
        bezierPath.addLineToPoint(CGPointMake(18.56, 1.41))
        bezierPath.closePath()
        
        theKeyOuter.frame = bounds
        theKeyOuter.path = bezierPath.CGPath
        theKeyOuter.fillColor = Colors().red
        
        addSublayer(theKeyOuter)
        
        
        let bezier2Path = UIBezierPath()
        bezier2Path.moveToPoint(CGPointMake(17.38, 12.64))
        bezier2Path.addLineToPoint(CGPointMake(14.06, 9.24))
        bezier2Path.addLineToPoint(CGPointMake(17.38, 5.84))
        bezier2Path.addLineToPoint(CGPointMake(16.31, 4.8))
        bezier2Path.addLineToPoint(CGPointMake(13.01, 8.17))
        bezier2Path.addLineToPoint(CGPointMake(9.71, 4.8))
        bezier2Path.addLineToPoint(CGPointMake(8.64, 5.84))
        bezier2Path.addLineToPoint(CGPointMake(11.96, 9.24))
        bezier2Path.addLineToPoint(CGPointMake(8.64, 12.64))
        bezier2Path.addLineToPoint(CGPointMake(9.71, 13.69))
        bezier2Path.addLineToPoint(CGPointMake(13.01, 10.32))
        bezier2Path.addLineToPoint(CGPointMake(16.31, 13.69))
        bezier2Path.addLineToPoint(CGPointMake(17.38, 12.64))
        bezier2Path.closePath()
        
        theKeyInner.frame = bounds
        theKeyInner.path = bezier2Path.CGPath
        theKeyInner.fillColor = Colors().red
        
        addSublayer(theKeyInner)
    }
}