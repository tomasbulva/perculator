import UIKit
import QuartzCore

class OBButton: CALayer {
    
    weak var onboarding: OnBoardingButton?
    
    let top = CATextLayer()
    let topstring = "?"
    
    let circle = CAShapeLayer()
    
    override func drawInContext(ctx: CGContext) {
        if let onboarding = onboarding {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        
        //println("OBButton setUpLayer")
        
        let screenWidth = onboarding!.screenWidth
        let screenHeight = onboarding!.screenHeight
        let margin = onboarding!.margin
        let buttonSize = onboarding!.buttonSize
        
        let BottomCornerX = onboarding!.BottomCornerX
        let BottomCornerY = onboarding!.BottomCornerY
        
        
        circle.path = UIBezierPath(roundedRect: bounds.insetBy(dx: 0.0, dy: 0.0), cornerRadius: buttonSize).CGPath
        circle.lineCap = kCALineCapButt
        circle.lineJoin = kCALineJoinMiter
        circle.lineWidth = 2.0
        circle.miterLimit = 0.0
        circle.strokeColor = Colors().darkBlue
        circle.opacity = 0.4
        circle.fillColor = Colors().noColor
        
        addSublayer(circle)
        
        
        top.frame = bounds.insetBy(dx: 0.0, dy: 0.0) //CGRect(x:0.0,y:0.0,width:buttonSize,height:buttonSize)
        top.foregroundColor = Colors().darkBlue
        top.opacity = 0.4
        top.fontSize = 15
        top.font = CTFontCreateWithName("AvenirNext-DemiBold", top.fontSize, nil)
        top.alignmentMode = kCAAlignmentCenter
        top.contentsScale = UIScreen.mainScreen().scale
        top.string = topstring

        addSublayer(top)
    }
    
    
}