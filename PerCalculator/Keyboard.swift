import UIKit
import QuartzCore

class Keyboard: CALayer {
    
    weak var calculator: Calculator?
    let keyboardData = [Key]()
    
    var keyLabel = [LabelRef]()
    var keyBgr = [BackgroundRef]()
    
    let background = CAShapeLayer()
    let delKey = DelKey()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func drawInContext(ctx: CGContext) {
        if let calculator = calculator {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        //println("Keyboard setUpLayer")
        if appDelegate.appWasInBackground == false {
            var topMargin = bounds.height * 0.25
            
            var i = 0
            var row = 0
            var column = 0
            let keywidth = bounds.width/3
            let keyheight = bounds.height/4
            
            for key in calculator!.keyboardData {
                
                if i%3 == 0 && i > 0 {
                    row++
                    column = 0
                }else if i > 0{
                    column++
                }
                
                var currBackground = CAShapeLayer()
                currBackground.frame = CGRect(x:keywidth*CGFloat(column),y:keyheight*CGFloat(row),width:keywidth,height:keyheight)
                currBackground.backgroundColor = Colors().darkBlue
                currBackground.opacity = key.color ? 0.05 : 0.02
                
                addSublayer(currBackground)
                
                keyBgr.append(BackgroundRef(layer: currBackground, frame: currBackground.frame, backgroundColor: currBackground.backgroundColor!))
                
                
                var currLabel = CATextLayer()
                currLabel.frame = CGRect(x:keywidth*CGFloat(column),y:keyheight*CGFloat(row)+(keyheight*0.3),width:keywidth,height:keyheight)
                
                currLabel.foregroundColor = Colors().red
                currLabel.opacity = key.keyname != "\u{232B}" ? 1 : 0
                
                currLabel.fontSize = currLabel.frame.height * 0.33
                currLabel.font = CTFontCreateWithName("AvenirNextCondensed-Regular", currLabel.fontSize, nil)
                currLabel.alignmentMode = kCAAlignmentCenter
                currLabel.contentsScale = UIScreen.mainScreen().scale
                currLabel.string = key.keyname
                
                addSublayer(currLabel)
                
                keyLabel.append(LabelRef(layer: currLabel, frame: currLabel.frame, foregroundColor: currLabel.foregroundColor, fontSize: currLabel.fontSize, font: currLabel.font, alignmentMode: currLabel.alignmentMode, contentsScale: currLabel.contentsScale, string: currLabel.string))
                
                if(key.keyname == "\u{232B}") {
                    addSublayer(delKey)
                }
                
                i++
            }
        }
    }
    
}