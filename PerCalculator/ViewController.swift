//
//  ViewController.swift
//  PerCalculator
//
//  Created by Tomas Bulva on 7/10/15.
//  Copyright (c) 2015 cchangeinc. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    // implement previous state
    var mainValue:String = ""
    var mainValueCache:String = ""
    
    let onboarding = OnBoarding(frame: CGRectZero)
    let onboardingbutton = OnBoardingButton(frame: CGRectZero)
    
    let background = Background(frame: CGRectZero)
    let slider = Slider(frame: CGRectZero)
    let display = Displays(frame: CGRectZero)
    let calculator = Calculator(frame: CGRectZero)
    let defaultsettings = DefaultSettings()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewWillAppear(animated: Bool) {
        // userDefaults settings for debug:
        // println( "userDefaults settings for debug: \(NSUserDefaults.standardUserDefaults().dictionaryRepresentation())" )
        appDelegate.versionString = version()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.addSubview(background)
        view.addSubview(display)
        view.addSubview(slider)
        view.addSubview(onboardingbutton)
        
        if !DefaultSettings().onboarding {
            view.addSubview(onboarding)
            onboardingbutton.animateButtonHide(1.0,delay:0.5,callback: {_ in
                //println("animateButtonShow callback")
            })
            onboarding.animateOnBoardingShow(1.0, delay: 0.0,callback: {_ in
                //println("animateOnBoardingShow callback")
            })
            DefaultSettings().saveOBState(true)
        }
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        //println("* viewDidAppear")
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let screenXCenter = CGRectGetMidX(UIScreen.mainScreen().bounds)
        let screenYCenter = CGRectGetMidY(UIScreen.mainScreen().bounds)
        let sliderHeight = 60.0;
        
        background.frame = CGRect(x: screenXCenter-screenHeight, y: screenYCenter-screenHeight, width: screenHeight*2, height: screenHeight*2)
        //backgound.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        //println("[viewDidLayoutSubviews] 9CGRect( x: \(screenXCenter-screenHeight), y: \(screenYCenter-screenHeight), width: \(screenHeight*2), height: \(screenHeight*2) )")
        
        display.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        display.topDisplay.tm.string = self.mainValue //"\(formatNumber(self.mainValue))"
        
        let sliderVerticalCenter = screenYCenter-CGFloat(sliderHeight/2)
        slider.frame = CGRect(x: 0, y: sliderVerticalCenter, width: screenWidth, height: CGFloat(sliderHeight))
        calculator.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        onboarding.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        onboardingbutton.frame = CGRect(x: onboardingbutton.BottomCornerX-50, y: onboardingbutton.BottomCornerY-50, width: onboardingbutton.buttonSize+50, height: onboardingbutton.buttonSize+50)
        
        //onboardingbutton.backgroundColor = UIColor(rgba: "#FEFEFE")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchViews", name: "topDisplayClick", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchMath", name: "BottomDisplayClick", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateDisplays:", name: "SliderValueChanged", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardUpdate:", name: "keyboardHit", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardViewButtons:", name: "keyboardViewButtons", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "helpClicked", name: "help", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onboardingClicked", name: "onboarding", object: nil)
        
        
        self.update()
        super.viewDidAppear(animated)
        
        //println("[viewDidLayoutSubviews] backgound.frame \(backgound.frame)")
        //println("[viewDidLayoutSubviews] screenWidth \(screenWidth)")
        //println("[viewDidLayoutSubviews] onboardingbutton.screenWidth \(onboardingbutton.screenWidth)")
        //println("[viewDidLayoutSubviews] onboardingbutton \(onboardingbutton.frame)")
    }
    
    func helpClicked(){
        DefaultSettings().saveNumState(0.5,main:("100" as NSString).doubleValue)
        onboardingbutton.animateButtonHide(1.0, delay: 0.0, callback: {})
        
        self.update()
        
        view.addSubview(onboarding)
        onboarding.animateOnBoardingShow(1.0, delay: 0.0, callback: {})
    }
    
    func onboardingClicked(){
        //println("onboarding clicked")
        onboarding.animateOnBoardingHide(1.0, delay: 0.0, callback: {_ in
            //println("animateCalcHide callback")
            self.onboarding.removeFromSuperview()
        })
        onboardingbutton.animateButtonShow(1.0,delay:0.5,callback: {_ in
            //println("animateButtonShow callback")
        })
    }
    
    func update(){
        var mainVal = "100"
        var percVal = 0.5
        display.origBottomFontSize = display.bottomDisplay.tm.fontSize
        display.originalBottomDisplayFrame = display.bottomDisplay.tm.frame
        //println("[update] mainVal test \( DefaultSettings().mainVal )")
        
        if DefaultSettings().mainVal != 0.0 {
            //println("[update] mainVal \(DefaultSettings().mainVal)")
            mainVal = "\(DefaultSettings().mainVal)"
        }
        
        let tmp = mainVal.rangeOfString(".0")
            
        if mainVal.rangeOfString(".0") != nil {
            //println("[update] mainVal decimal exists \( tmp )")
            //advance(self.mainValue.endIndex, -2)
            self.mainValue = mainVal.substringWithRange(Range<String.Index>(start: mainVal.startIndex, end: tmp!.startIndex))
            //println("[update] mainVal decimal exists \( self.mainValue )")
        }else{
            self.mainValue = mainVal
        }
        
        //println("[update] percVal test \( DefaultSettings().percVal )")
        
        if DefaultSettings().percVal != 0.0 {
            percVal = DefaultSettings().percVal
            //println("[update] percVal \(DefaultSettings().percVal)")
        }
        
        slider.lowerValue = percVal
        //println("percVal \(percVal)")
        if percVal > 0.15 && slider.percHidden == true {
            slider.animatePositionIn()
        }
        slider.lowerThumbLayer.SliderValueChanged(slider)
        NSNotificationCenter.defaultCenter().postNotificationName("SliderValueChanged", object:nil, userInfo:["value":slider.lowerValue])
    }
    
    func switchViews(){
        if (appDelegate.inmotion == false){
            appDelegate.inmotion = true
            display.status = background.transition
            
            //println("[switchViews] appDelegate.inmotion \(appDelegate.inmotion) **switching view**")
            
            // calc -> slider
            if background.transition{
                calculator.animateCalcHide(0.5, delay: 1.0, callback: {_ in
                    self.background.backgroundTransitionHide(0.5, delay: 0.5, callback: {_ in
                        self.slider.animateSliderShow(0.5, delay: 0.5,callback:{_ in
                            //println("self.slider.lowerValue \(self.slider.lowerValue)")
                            if self.slider.lowerValue > 0.15 {
                                self.slider.animatePositionIn()
                            }
                        })
                        self.slider.showThumbLayer(0.5, delay: 0.35)
                        self.display.animateDisplayShow(0.5, delay: 0.0)
                        self.onboardingbutton.animateButtonShow(0.5, delay: 0.0, callback: {_ in self.onboarding.removeFromSuperview() })
                        NSNotificationCenter.defaultCenter().postNotificationName("SliderValueChanged", object:nil, userInfo:["value":self.slider.lowerValue])
                        self.appDelegate.calcScreen = false
                        NSTimer.scheduledTimerWithTimeInterval(0.75, target:self, selector: Selector("sliderShowFinished"), userInfo: nil, repeats: false)
                    })
                })
                
            }else{
                // slider -> calc
                mainValueCache = mainValue
                slider.animatePositionOut()
                slider.animateSliderHide(0.5, delay: 0.0)
                slider.hideThumbLayer(0.5, delay: 0.35)
                background.backgroundTransitionShow(0.5, delay: 1.0, callback: {_ in
                    self.onboardingbutton.animateButtonHide(0.5, delay: 0.0, callback: {})
                    self.display.animateDisplayHide(0.5, delay: 0.0)
                    NSTimer.scheduledTimerWithTimeInterval(0.75, target:self, selector: Selector("attachCalc"), userInfo: nil, repeats: false)
                    self.appDelegate.calcScreen = true
                })
                
            }
        }else{
            //println("[switchViews] appDelegate.inmotion \(appDelegate.inmotion) **NOT switching view**")
        }
    }
    
    func attachCalc(){
        self.view.addSubview(self.calculator)
        NSTimer.scheduledTimerWithTimeInterval(0.3, target:self, selector: Selector("animAttachCalc"), userInfo: nil, repeats: false)
    }
    
    func animAttachCalc(){
        self.calculator.animateCalcShow(1.0, delay:0.0, callback: {_ in
            self.appDelegate.inmotion = false
        })
    }
    
    func sliderShowFinished(){
        //println("[sliderShowFinished] triggered")
        self.appDelegate.inmotion = false
    }
    
    func switchMath(){
        
        //println("********* appDelegate.inmotion \(appDelegate.inmotion)")
        
        if (appDelegate.inmotion == false){
            background.backgroundColorTransition(0.5,delay:0.0, callback: {_ in
                self.background.backTrans.state = self.background.backTrans.state == false ? true : false
                self.display.bottomDisplayTLString(self.background.backTrans.state, sliderVal: self.slider.lowerValue, origAmount: self.mainValue)
                NSNotificationCenter.defaultCenter().postNotificationName("SliderValueChanged", object:nil, userInfo:["value": self.slider.lowerValue])
            })
        }else{
            //println("[switchMath] appDelegate.inmotion \(appDelegate.inmotion)")
        }
    }
    
    func percentages(sliderVal:Double) -> Double {
        
        let main = (self.mainValue as NSString).doubleValue
        let sld = round(sliderVal*100)/100
        
        let tmp1 = main * sld
        let tmp2 = main - tmp1
        
        //println("[percentages] background.backTrans.state \(background.backTrans.state)")
        
        let val = background.backTrans.state == false ? tmp1 : tmp2
        
        return val
    }
    
    func updateDisplays(notification:NSNotification){
        let sliderValue = notification.userInfo!["value"] as! Double
        display.topDisplay.tm.string = self.mainValue
        
        display.bottomDisplayTLString(background.backTrans.state, sliderVal: slider.lowerValue, origAmount: mainValue)
        
        let tmp = percentages(sliderValue)
        
        if String(format: "%.1f", tmp) == String(format: "%.1f", round( tmp )) {
            let valString = String(format:"%.0f", tmp)
            display.bottomDisplay.tm.string = valString
        }else{
            let testOfDecimal = String(format:"%.2f", tmp)
            let zero = testOfDecimal.substringWithRange(Range<String.Index>(start: testOfDecimal.endIndex.advancedBy(-1), end: testOfDecimal.endIndex))
            
            if zero == "0" {
                display.bottomDisplay.tm.string = String(format:"%.1f", tmp)
            }else{
                display.bottomDisplay.tm.string = String(format:"%.2f", tmp)
            }
        }
        
//        if count("\(display.bottomDisplay.tm.string)") >= 8 {
//            if display.bottomDisplay.tm.frame.origin.y == display.originalBottomDisplayFrame.origin.y {
//                display.displaySmallerType(0.3,delay:0.0,callback:{_ in
//                    println("smaller display callback")
//                    println("display.bottomDisplay.tm.frame.origin.y \(self.display.bottomDisplay.tm.frame)")
//                })
//            }
//        }else{
//            if display.bottomDisplay.tm.frame.origin.y != display.originalBottomDisplayFrame.origin.y {
//                display.displayBiggerType(0.3,delay:0.0,callback:{_ in
//                    println("bigger display callback")
//                    println("display.bottomDisplay.tm.frame.origin.y \(self.display.bottomDisplay.tm.frame)")
//                })
//            }
//        }
        
        //println("display.bottomDisplay.tm.fontSize \(display.bottomDisplay.tm.fontSize)");
        
        DefaultSettings().saveNumState(sliderValue,main:(self.mainValue as NSString).doubleValue)
    }
    
    func keybordColorReset(timer: NSTimer){
        //println("Keyboard array index \(timer.userInfo as! Int)")
        calculator.keyboard.keyBgr[timer.userInfo as! Int ].layer.opacity = calculator.keyboardData[timer.userInfo as! Int ].color ? 0.05 : 0.02
    }
    
    func keyboardUpdate(notification:NSNotification){
        let keyValue = notification.userInfo!["key"] as! String
        let keyIndex = notification.userInfo!["keyIndex"] as! Int
        
        var currScreen = "\(mainValue)"

        calculator.keyboard.keyBgr[keyIndex].layer.opacity = 0.08 //calculator.keyboardData[keyIndex].color ? 0.08 : 0.08
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "keybordColorReset:", userInfo: keyIndex, repeats: false)
        
        switch keyValue {
            case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" :
                if currScreen.rangeOfString(".") != nil {
                    let decimalPoints = currScreen.substringWithRange(Range<String.Index>(start: currScreen.rangeOfString(".")!.startIndex, end: currScreen.endIndex))
                    
                    //println("lenth of the decmal space: \(count(decimalPoints))")
                    
                    if decimalPoints.characters.count < 3 {
                        currScreen = String(currScreen+keyValue)
                        self.mainValue = currScreen
                    }
                }else if String(currScreen+keyValue).characters.count < 8 && currScreen == "0"{
                    currScreen = keyValue
                    self.mainValue = currScreen
                    //println("option: 1.2")
                }else if String(currScreen+keyValue).characters.count < 8 && currScreen != "0" {
                    currScreen = String(currScreen+keyValue)
                    self.mainValue = currScreen
                    //println("option: 1.1")
                }
            case "." :
                if currScreen.rangeOfString(".") == nil && currScreen.characters.count < 6 {
                    currScreen = String(currScreen+keyValue)
                    self.mainValue = currScreen
                    //println("option: 2.1")
                }else if currScreen == "0" && currScreen.characters.count < 6 {
                    currScreen = "0."
                    self.mainValue = currScreen
                    //println("option: 2.2")
                }
            case "\u{232B}" :
                if currScreen.characters.count == 1 {
                    currScreen = "0"
                    self.mainValue = currScreen
                    //println("option: 3.1")
                }else{
                    currScreen = currScreen.substringWithRange(Range<String.Index>(start: currScreen.startIndex, end: currScreen.endIndex.advancedBy(-1)))
                    self.mainValue = currScreen
                    //println("option: 3.2")
                }
            default :
                currScreen = String(currScreen)
                self.mainValue = currScreen
                //println("option: 4")
        }
        
        
        display.topDisplay.tm.string = currScreen
        //println("mainValue: \(mainValue)");
        
    }
    
    func handleKeyboardViewButtons(notification:NSNotification){
        let whatButton = notification.userInfo!["key"] as! String
        if whatButton == "cancel" {
            switchViews()
            mainValue = mainValueCache
        }
        if whatButton == "perculate" {
            switchViews()
            
            if let displayString = display.topDisplay.tm.string as? String {
                if let idx = displayString.characters.indexOf(".") {
                    let pos = displayString.startIndex.distanceTo(idx)
                    let endpos = displayString.characters.count-1
                    if(pos == endpos) {
                        let newstring = displayString.substringWithRange(Range<String.Index>(start: displayString.startIndex, end: displayString.endIndex.advancedBy(-1)))
                        //println("shit's equal \(newstring)")
                        display.topDisplay.tm.string = newstring
                        mainValue = newstring
                    }
                    //println("Found position \(pos) string length \(endpos)")
                }else{
                    //println("not-Found")
                }
            }
            
            
        }
    }
    
    func formatNumber(number:Double) -> Double {
        return Double(floor(100*number)/100);
    }
    
    func displayValue(val:String){
        let intVal = (val as NSString).floatValue
        if floor( intVal ) == intVal {
            if String(val) != String(stringInterpolationSegment: display.topDisplay.tm.string) {
                display.topDisplay.tm.string = val
            }
        }else{
            if String(val) != String(stringInterpolationSegment: display.topDisplay.tm.string) {
                display.topDisplay.tm.string = val.substringWithRange(Range<String.Index>(start: val.startIndex, end: val.endIndex.advancedBy(-2)))
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func version() -> String {
        let dictionary = NSBundle.mainBundle().infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) build \(build)"
    }


}

