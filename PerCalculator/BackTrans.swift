import UIKit
import QuartzCore

class BackTrans: CALayer {
    
    //let colors = Colors()
    let l = CAShapeLayer()
    weak var background: Background?
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var state: Bool = false {
        didSet {
            //animateState()
        }
    }
    
    override func drawInContext(ctx: CGContext) {
        if let background = background {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        //println("BackTrans setUpLayer appDelegate.appWasInBackground \(appDelegate.appWasInBackground)")
        if appDelegate.appWasInBackground == false {
            l.frame = bounds
            l.backgroundColor = state == false ? Colors().blue : Colors().yellow
            l.anchorPoint = CGPointMake(0.5, 0);
            //println("BackTrans setUpLayer executed frame \(l.frame) anchorPoint \(l.anchorPoint)")
            addSublayer(l)
        }
    }
    
    func animate(){
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = 0
        rotation.toValue = degree2radian(-180)
        
        let colorChange = CABasicAnimation(keyPath: "backgroundColor")
        colorChange.fromValue = state == false ? Colors().blue : Colors().yellow
        colorChange.toValue = Colors().red
        
        let group = CAAnimationGroup()
        group.duration = 1
        group.beginTime = CACurrentMediaTime()+0.3
        group.removedOnCompletion = false
        group.fillMode = kCAFillModeForwards
        group.animations = [rotation, colorChange]
        group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        l.addAnimation(group, forKey: nil)
        
        //l.addAnimation(rotation, forKey: "rotationAnimation")
    }
    
    func animateBack(){
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = degree2radian(-180)
        rotation.toValue = 0
        
        let colorChange = CABasicAnimation(keyPath: "backgroundColor")
        colorChange.fromValue = Colors().red
        colorChange.toValue = state == false ? Colors().blue : Colors().yellow
        
        let group = CAAnimationGroup()
        group.duration = 1
        group.beginTime = CACurrentMediaTime()+0.3
        group.removedOnCompletion = false
        group.fillMode = kCAFillModeForwards
        group.animations = [rotation, colorChange]
        group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        l.addAnimation(group, forKey: nil)
    }
    
    func animateState(){
        let stateChange = CABasicAnimation(keyPath: "backgroundColor")
        stateChange.fromValue = state == false ? Colors().yellow : Colors().blue
        stateChange.toValue = state == false ? Colors().blue : Colors().yellow
        stateChange.duration = 1
        stateChange.removedOnCompletion = false
        stateChange.fillMode = kCAFillModeForwards
        stateChange.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        l.addAnimation(stateChange, forKey: "stateChange")
    }
    
    
    func degree2radian(a:CGFloat)->CGFloat {
        let b = CGFloat(M_PI) * a/180
        return b
    }
    
    
}