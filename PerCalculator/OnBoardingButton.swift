import UIKit
import QuartzCore

class OnBoardingButton: UIControl {
    
    let obbutton = OBButton()
    let barea = OBButtonAA()
    
    var previousLocation = CGPoint()
    var status = false;
    
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    let screenHeight = UIScreen.mainScreen().bounds.size.height
    let margin: CGFloat = 10.0
    let buttonSize: CGFloat = 20.0
    
    var BottomCornerX:CGFloat = 0.0
    var BottomCornerY:CGFloat = 0.0
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        BottomCornerX = screenWidth - (margin - buttonSize)
        BottomCornerY = screenHeight - (margin - buttonSize)
        
        obbutton.onboarding = self
        layer.addSublayer(obbutton)
        
        barea.onboarding = self
        layer.addSublayer(barea)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        obbutton.frame = CGRect(x: 0.0, y: 0.0, width: buttonSize, height: buttonSize)
        obbutton.setNeedsDisplay()
        
        barea.frame = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
        barea.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        previousLocation = touch.locationInView(self)
        if barea.frame.contains(previousLocation) {
            NSNotificationCenter.defaultCenter().postNotificationName("help", object:nil)
        }
        return true
    }
    
    func animateButtonHide(time:NSTimeInterval,delay:NSTimeInterval, callback: ()->Void) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: [],
            animations: {
                self.obbutton.opacity = 0
            },
            completion: {_ in
                callback()
            }
        )
    }
    
    func animateButtonShow(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: [],
            animations: {
                self.obbutton.opacity = 1
            },
            completion: {_ in
                callback()
            }
        )
    }
}