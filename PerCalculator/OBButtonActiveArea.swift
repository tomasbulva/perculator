import UIKit
import QuartzCore

class OBButtonAA: CALayer {
    
    weak var onboarding: OnBoardingButton?

    let area = CAShapeLayer()
    
    override func drawInContext(ctx: CGContext) {
        if let onboarding = onboarding {
            setUpLayer()
        }
    }
    
    func setUpLayer() {

        area.path = UIBezierPath(rect: bounds.insetBy(dx: 0.0, dy: 0.0)).CGPath
        area.lineCap = kCALineCapButt
        area.lineJoin = kCALineJoinMiter
        area.lineWidth = 0.0
        area.miterLimit = 0.0
        area.opacity = 1.0
        area.fillColor = Colors().noColor
        
        addSublayer(area)
    }
    
    
}