import UIKit
import QuartzCore

class SliderTrackPatternBeadsLayer: CALayer {
    weak var slider: Slider?
    
    override func drawInContext(ctx: CGContext) {
        if let slider = slider {
            
            //let zeroPointLocation = slider.thumbFrame
            let beadsize = bounds.height/6
            let stageWidth = bounds.width - 50
            let increment = CGFloat(stageWidth) * 0.24
            let startPoint = (bounds.width - stageWidth) / 2
            var xpos = CGFloat(0)
            
            var i = 0
            while i < 5 {
                
                if i == 0 {
                    xpos += startPoint
                }else{
                    xpos += increment
                }
                
                let rect = CGRect(x: xpos, y: (bounds.height-beadsize)/2, width: beadsize, height: beadsize)
                let thumbPath = UIBezierPath(ovalInRect: rect)
                
                CGContextSetFillColorWithColor(ctx, Colors().red)
                CGContextAddPath(ctx, thumbPath.CGPath)
                CGContextFillPath(ctx)
                
                i++
            }
        }
    }
}