import UIKit
import QuartzCore

class Displays: UIControl {
    
    let topDisplay = TopDisplay()
    let bottomDisplay = BottomDisplay()
    
    var previousLocation = CGPoint()
    var status = false;
    
    var origBottomFontSize:CGFloat = 0.0
    var originalBottomDisplayFrame:CGRect = CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        topDisplay.displays = self
        topDisplay.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(topDisplay)
        
        bottomDisplay.displays = self
        bottomDisplay.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(bottomDisplay)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        
        let topHalfOfScreen = CGRect(x:0,y:0,width:screenWidth,height:screenHeight/2)
        let bottomHalfOfScreen = CGRect(x:0,y:screenHeight/2,width:screenWidth,height:screenHeight/2)
        
        topDisplay.frame = topHalfOfScreen
        topDisplay.setNeedsDisplay()
        
        bottomDisplay.frame = bottomHalfOfScreen
        bottomDisplay.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        previousLocation = touch.locationInView(self)
        
        
        // Hit test the thumb layers
        if topDisplay.frame.contains(previousLocation) {
            //println("1 beginTrackingWithTouch")
            NSNotificationCenter.defaultCenter().postNotificationName("topDisplayClick", object: nil)
            //sendActionsForControlEvents(.TouchUpInside)
        }
        
        if bottomDisplay.frame.contains(previousLocation) {
            //println("2 beginTrackingWithTouch")
            NSNotificationCenter.defaultCenter().postNotificationName("BottomDisplayClick", object: nil)
        }
        
        return true //bottomDisplay.clicked //|| upperThumbLayer.highlighted
    }
    
    func animateDisplayHide(time:NSTimeInterval,delay:NSTimeInterval) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: [],
            animations: {_ in
                self.topDisplay.tm.foregroundColor = Colors().white
                self.bottomDisplay.opacity = 0
                self.topDisplay.tl.opacity = 0
            },
            completion: {_ in
                //self.topDisplayTLString()
            }
        )
    }
    
    func animateDisplayShow(time:NSTimeInterval,delay:NSTimeInterval) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {_ in
                self.topDisplay.tm.foregroundColor = Colors().red
                self.bottomDisplay.opacity = 1
                self.topDisplay.tl.opacity = 0.4
            },
            completion: {_ in
                //self.topDisplayTLString()
            }
        )
    }
    
    func topDisplayTLString(){
        if status == true {
            topDisplay.tl.string = "original amount"
        }else{
            topDisplay.tl.string = ""
        }
    }
    
    func displaySmallerType(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
        //println("[displaySmallerType] self.bottomDisplay.tm.frame.origin.y \(self.bottomDisplay.tm.frame.origin.y)")
        self.bottomDisplay.tm.fontSize = self.origBottomFontSize*0.75
        UIView.animateWithDuration(time,
            delay: delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {_ in
                self.bottomDisplay.tm.frame.origin.y = self.originalBottomDisplayFrame.origin.y+20
                
                self.bottomDisplay.tm.font = CTFontCreateWithName("AvenirNextCondensed-Regular", self.bottomDisplay.tm.fontSize, nil)
            },
            completion: {_ in
                callback()
            }
        )
    }
    
    func displayBiggerType(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
        //println("[displayBiggerType] self.bottomDisplay.tm.frame.origin.y \(self.bottomDisplay.tm.frame.origin.y)")
        
        self.bottomDisplay.tm.fontSize = self.origBottomFontSize
        UIView.animateWithDuration(time,
            delay: delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {_ in
                self.bottomDisplay.tm.frame.origin.y = self.originalBottomDisplayFrame.origin.y
                self.bottomDisplay.tm.font = CTFontCreateWithName("AvenirNextCondensed-Regular", self.bottomDisplay.tm.fontSize, nil)
                
            },
            completion: {_ in
                callback()
            }
        )
    }
    
    func bottomDisplayTLString(backgroundstatus:Bool,sliderVal:Double,origAmount:String){
        
        let percent = String(format: "%0.0f", (sliderVal*100))
        
        if backgroundstatus == true {
            bottomDisplay.tl.string = "\(percent)% off \(origAmount)"
        }else{
            bottomDisplay.tl.string = "\(percent)% of \(origAmount)"
        }
    }

    
    
}