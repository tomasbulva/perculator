import UIKit
import QuartzCore

class OnBoarding: UIControl {
    
    let graphics = Graphics()
    
    var previousLocation = CGPoint()
    var status = false;
    
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    let screenHeight = UIScreen.mainScreen().bounds.size.height
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        graphics.onboarding = self
        graphics.opacity = 0
        layer.addSublayer(graphics)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        graphics.frame = CGRect(x:0.0,y:0.0,width:screenWidth,height:screenHeight)
        graphics.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        previousLocation = touch.locationInView(self)
        if graphics.frame.contains(previousLocation) {
            NSNotificationCenter.defaultCenter().postNotificationName("onboarding", object:nil)
        }
        return true
    }
    
    func animateOnBoardingHide(time:NSTimeInterval,delay:NSTimeInterval, callback: ()->Void) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: [],
            animations: {
                self.graphics.opacity = 0
            },
            completion:{_ in
                callback()
            }
        )
    }
    
    func animateOnBoardingShow(time:NSTimeInterval,delay:NSTimeInterval, callback: ()->Void) {
        UIView.animateWithDuration(time,
            delay: delay,
            options: [],
            animations: {
                self.graphics.opacity = 1
            },
            completion: {_ in
                callback()
            }
        )
    }
}