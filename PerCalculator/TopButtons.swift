import UIKit
import QuartzCore

class TopButtons: CALayer {
    
    weak var calculator: Calculator?
    let cancelButton = CATextLayer()
    let percalculateButton = CATextLayer()
    
    let cancelButtonTouchArea = CAShapeLayer()
    let percalculateButtonTouchArea = CAShapeLayer()
    
    
    override func drawInContext(ctx: CGContext) {
        if let calculator = calculator {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        
        //println("TopButtons setUpLayer")
        
        let buttonHeight = CGFloat(44.0)
        let buttonWidth = CGFloat(100.0)
        let margin = CGFloat(10.0)
        let postionY = bounds.height - CGFloat(buttonHeight) - margin
        
        cancelButtonTouchArea.frame = CGRect(x:margin,y:postionY,width:buttonWidth,height:buttonHeight)
        cancelButtonTouchArea.backgroundColor = Colors().noColor
        
        addSublayer(cancelButtonTouchArea)
        
        
        cancelButton.frame = CGRect(x:margin,y:postionY+12,width:buttonWidth,height:buttonHeight-12)
        cancelButton.foregroundColor = UIColor(rgba: "#fff").CGColor
        cancelButton.fontSize = frame.height * 0.04
        cancelButton.font = CTFontCreateWithName("AvenirNext-Bold", cancelButton.fontSize, nil)
        cancelButton.alignmentMode = kCAAlignmentCenter
        cancelButton.contentsScale = UIScreen.mainScreen().scale
        cancelButton.string = "cancel"
        
        addSublayer(cancelButton)
        
        
        percalculateButtonTouchArea.frame = CGRect(x:bounds.width-buttonWidth-margin,y:postionY,width:buttonWidth,height:buttonHeight)
        percalculateButtonTouchArea.backgroundColor = Colors().noColor
        
        addSublayer(percalculateButtonTouchArea)
        
        percalculateButton.frame = CGRect(x:bounds.width-buttonWidth-margin,y:postionY+12,width:buttonWidth,height:buttonHeight-12)
        percalculateButton.foregroundColor = UIColor(rgba: "#fff").CGColor
        percalculateButton.fontSize = frame.height * 0.04
        percalculateButton.font = CTFontCreateWithName("AvenirNext-Bold", percalculateButton.fontSize, nil)
        percalculateButton.alignmentMode = kCAAlignmentCenter
        percalculateButton.contentsScale = UIScreen.mainScreen().scale
        percalculateButton.string = "calculate"
        
        addSublayer(percalculateButton)
    }
    
}