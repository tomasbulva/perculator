import UIKit
import QuartzCore

class BottomDisplay: CALayer {
    
    //override var frame: CGRect
    let tm = CATextLayer() // Text main
    let tl = CATextLayer() // Text label
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    var number2display:Double = 100.00
    weak var displays: Displays?
    
    var clicked: Bool = false {
        didSet {
            //setNeedsDisplay()
            //println("BottomDisplay clicked")
        }
    }
    
    override func drawInContext(ctx: CGContext) {
        if let displays = displays {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        
        if appDelegate.appWasInBackground == false {
            //println("=== BottomDisplay setUpLayer ===")
            
            let topMargin = bounds.height * 0.25
            let displayHeight = frame.height * 0.25
            //println("displayHeight \(displayHeight)")
            tm.anchorPoint = CGPointMake(0, 0.5);
            tm.frame = CGRect(x:0,y:topMargin,width:bounds.width,height:displayHeight+10)
            tm.foregroundColor = Colors().white
            tm.fontSize = displayHeight
            tm.font = CTFontCreateWithName("AvenirNextCondensed-Regular", tm.fontSize, nil)
            tm.alignmentMode = kCAAlignmentCenter
            tm.contentsScale = UIScreen.mainScreen().scale
            tm.string = String(format: "%0f", number2display)
            
            addSublayer(tm)
            
            //println("tm.bounds.height \(tm.frame.origin.y)")
            
            tl.frame = CGRect(x:0,y:tm.frame.origin.y+tm.bounds.height,width:bounds.width,height:bounds.height)
            tl.foregroundColor = Colors().darkBlue
            tl.opacity = 0.4
            tl.fontSize = frame.height * 0.04
            tl.font = CTFontCreateWithName("AvenirNext-DemiBold", tm.fontSize, nil)
            tl.alignmentMode = kCAAlignmentCenter
            tl.contentsScale = UIScreen.mainScreen().scale
            tl.string = "percentage of"
            
            addSublayer(tl)
            
        }
    }
    
}