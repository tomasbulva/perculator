//
//  defaultsettings.swift
//  Perculator
//
//  Created by Tomas Bulva on 8/13/15.
//  Copyright (c) 2015 cchangeinc. All rights reserved.
//

import UIKit
import Foundation

class DefaultSettings {
    let onboarding = NSUserDefaults.standardUserDefaults().boolForKey("onboarding")
    let mainVal = NSUserDefaults.standardUserDefaults().doubleForKey("mainval")
    let percVal = NSUserDefaults.standardUserDefaults().doubleForKey("percval")
    
    func saveOBState(val:Bool) -> Void {
        NSUserDefaults.standardUserDefaults().setBool(val, forKey: "onboarding")
    }
    
    func saveNumState(perc:Double,main:Double) -> Void {
        NSUserDefaults.standardUserDefaults().setDouble(main, forKey: "mainval")
        NSUserDefaults.standardUserDefaults().setDouble(perc, forKey: "percval")
    }
}