import UIKit
import QuartzCore
import Foundation

class SliderPercentChar: CALayer {
    
    weak var slider: Slider?
    var percentLayer = CATextLayer()
    var backgroundLayer = CAShapeLayer()
    
    
    override func drawInContext(ctx: CGContext) {
        
        if let slider = slider {
            
            //self.backgroundLayer.frame = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
            let radius: CGFloat = bounds.height
            //self.backgroundLayer.bounds = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
            self.backgroundLayer.path = UIBezierPath(roundedRect: CGRect(x: 0.0, y: 0.0, width: radius, height: radius)  , cornerRadius: radius).CGPath
            self.backgroundLayer.position = CGPoint(x: 0.0, y: 0.0)

            //CGRectGetMidX(self.frame) - radius/2
            //CGRectGetMidY(self.frame) - radius/2
            
            self.backgroundLayer.fillColor = Colors().red
            //self.backgroundLayer.anchorPoint = CGPointMake( 0.5, 0.5 )
            
            addSublayer(self.backgroundLayer)
            
            // =====================
            
            //self.percentLayer.frame = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
            //self.percentLayer.bounds = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
            self.percentLayer.foregroundColor = Colors().white
            self.percentLayer.fontSize = bounds.height/2
            self.percentLayer.font = CTFontCreateWithName("AvenirNext-DemiBold", self.percentLayer.fontSize, nil)
            self.percentLayer.alignmentMode = kCAAlignmentCenter
            self.percentLayer.contentsScale = UIScreen.mainScreen().scale
            self.percentLayer.string = "%"
            //self.percentLayer.anchorPoint = CGPointMake( 0.5, 0.5 );
            
            addSublayer(self.percentLayer)
            
        }
    }
}