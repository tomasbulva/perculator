//
//  Colors.swift
//  Perculator
//
//  Created by Tomas Bulva on 8/6/15.
//  Copyright (c) 2015 cchangeinc. All rights reserved.
//

import UIKit
import Foundation

class Colors {
    var white = UIColor(rgba: "#FEFEFE").CGColor
    var red = UIColor(rgba: "#F43C3B").CGColor
    var blue = UIColor(rgba: "#2AE5F4").CGColor
    var yellow = UIColor(rgba: "#FED533").CGColor
    var darkBlue = UIColor(rgba: "#2E2C49").CGColor

    var grayDark = UIColor(rgba: "#FAFAFA").CGColor
    var grayLight = UIColor(rgba: "#FEFEFE").CGColor
    
    var noColor = UIColor(rgba: "#FEFEFE").colorWithAlphaComponent(0.0).CGColor
}