import UIKit
import QuartzCore
import Foundation

class Key: NSObject {
    let keyname: String
    let color: Bool
    
    init(keyname: String, color: Bool) {
        self.keyname = keyname
        self.color = color
    }
}

class BackgroundRef {
    let layer: CAShapeLayer
    let frame: CGRect
    let backgroundColor: CGColor
    
    init(layer: CAShapeLayer, frame: CGRect ,backgroundColor: CGColor){
        self.layer = layer
        self.frame = frame
        self.backgroundColor = backgroundColor
    }
}

class LabelRef {
    let layer: CATextLayer!
    let frame: CGRect
    let foregroundColor: CGColor!
    let fontSize: CGFloat
    let font: AnyObject!
    let alignmentMode: String!
    let contentsScale: CGFloat
    let string: AnyObject!
    
    init(layer: CATextLayer!, frame: CGRect, foregroundColor: CGColor!, fontSize: CGFloat, font: AnyObject!, alignmentMode: String!, contentsScale: CGFloat, string: AnyObject!){
        self.layer = layer
        self.frame = frame
        self.foregroundColor = foregroundColor
        self.fontSize = fontSize
        self.font = font
        self.alignmentMode = alignmentMode
        self.contentsScale = contentsScale
        self.string = string
    }
}