import UIKit
import QuartzCore

class SliderThumbLayer: CALayer {
    
    var highlighted: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var textLayer = CATextLayer()
    
    weak var slider: Slider?
    
    func SliderValueChanged(slider: Slider) {
        self.textLayer.string = String(format: "%0.0f", (slider.lowerValue*100))
    }
    
    override func drawInContext(ctx: CGContext) {
        self.textLayer.foregroundColor = Colors().darkBlue
        self.textLayer.opacity = 1
        self.textLayer.fontSize = bounds.height * 0.33
        self.textLayer.font = CTFontCreateWithName("AvenirNextCondensed-Bold", self.textLayer.fontSize, nil)
        self.textLayer.alignmentMode = kCAAlignmentCenter
        self.textLayer.frame = CGRect(x: bounds.origin.x, y: bounds.origin.y+bounds.height * 0.28, width: bounds.width, height: bounds.height-bounds.height * 0.28)
        self.textLayer.contentsScale = UIScreen.mainScreen().scale
        
        addSublayer(self.textLayer)
        
        slider!.addTarget(self, action: "SliderValueChanged:", forControlEvents: .ValueChanged)
        
        if let slider = slider {
            let thumbFrame = bounds.insetBy(dx: bounds.height * 0.06, dy: bounds.height * 0.06)
            //println("thumbFrame \(thumbFrame)")
            let cornerRadius = thumbFrame.height * 1.0 / 2.0
            let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
            
            // Fill - with a subtle shadow
            CGContextSetFillColorWithColor(ctx, Colors().white)
            CGContextAddPath(ctx, thumbPath.CGPath)
            CGContextFillPath(ctx)
            
            // Outline
            CGContextSetStrokeColorWithColor(ctx, Colors().red)
            
            CGContextSetLineWidth(ctx, bounds.height * 0.12)
            CGContextAddPath(ctx, thumbPath.CGPath)
            CGContextStrokePath(ctx)
            
            if highlighted {
                CGContextSetFillColorWithColor(ctx, Colors().white)
                //transform = CATransform3DMakeTranslation(1.2, 1.2, 0.0)
            }
        }
    }
}