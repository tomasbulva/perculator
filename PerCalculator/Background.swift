import UIKit
import QuartzCore

class Background: UIView {
    let backTrans = BackTrans()

    var transition: Bool = false
    var inmotion: Bool = false
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override var frame: CGRect
    {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backTrans.background = self
        backTrans.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(backTrans)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        backTrans.frame = bounds.insetBy(dx: 0.0, dy: 0.0)
        backTrans.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    func backgroundTransitionShow(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
//        println("** backgroundTransitionShow")
//        if(appDelegate.inmotion == false){
//            appDelegate.inmotion = true
            //println("[backgroundTransitionShow] animation in progress...")
            UIView.animateWithDuration(time,
                delay: delay,
                options: UIViewAnimationOptions.CurveEaseOut,
                animations: {_ in
                    self.backTrans.transform = CATransform3DRotate(self.backTrans.transform, self.degree2radian(-180), 0, 0, 1)
                    self.backTrans.l.backgroundColor = Colors().red
                },
                completion: {_ in
                    self.transition = true
                    //self.appDelegate.inmotion = false
                    callback()
                }
            )
//        }else{
//            println("[backgroundTransitionShow] appDelegate.inmotion \(appDelegate.inmotion)")
//        }
    }
    
    func backgroundTransitionHide(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
        //println("** backgroundTransitionHide")
//        if(appDelegate.inmotion == false){
//            appDelegate.inmotion = true
            UIView.animateWithDuration(time,
                delay: delay,
                options: UIViewAnimationOptions.CurveEaseOut,
                animations: {_ in
                    self.backTrans.transform = CATransform3DRotate(self.backTrans.transform, self.degree2radian(180), 0, 0, 1)
                    self.backTrans.l.backgroundColor = self.backTrans.state == false ? Colors().blue : Colors().yellow
                },
                completion: {_ in
                    self.transition = false
                    //self.appDelegate.inmotion = false
                    callback()
                }
            )
//        }else{
//            println("[backgroundTransitionHide] self.inmotion \(appDelegate.inmotion)")
//        }
    }
    
    func backgroundColorTransition(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
        //println("** backgroundColorTransition")
        if(appDelegate.inmotion == false){
            appDelegate.inmotion = true
            
            UIView.animateWithDuration(time,
                delay: delay,
                options: .CurveEaseOut,
                animations: {
                    self.backTrans.l.backgroundColor = self.backTrans.state == false ? Colors().yellow : Colors().blue
                },
                completion: {_ in
                    self.appDelegate.inmotion = false
                    callback()
                }
            )
        }else{
            //println("[backgroundColorTransition] appDelegate.inmotion \(appDelegate.inmotion)")
        }
    }
    
    func degree2radian(a:CGFloat)->CGFloat {
        let b = CGFloat(M_PI) * a/180
        return b
    }
    
}