import UIKit
import QuartzCore

class Slider: UIControl {
    
    let trackLayer = SliderTrackLayer()
    let backgroundLayer = SliderTrackPatternLayer()
    let background2Layer = SliderTrackPatternBeadsLayer()
    let percentChar = SliderPercentChar()
    let lowerThumbLayer = SliderThumbLayer()
    var previousLocation = CGPoint()
    var oldPos:CGFloat = 0.0
    var trackwidth:CGFloat = 0.0
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var minimumValue: Double = 0.0 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var maximumValue: Double = 1.0 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var lowerValue: Double = 0.5 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var upperValue: Double = 1.0 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var trackTintColor = Colors().white {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    var trackHighlightTintColor = Colors().red {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    var thumbTintColor = Colors().white {
        didSet {
            lowerThumbLayer.setNeedsDisplay()
        }
    }
    
    var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    
    var perCharWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundLayer.slider = self
        backgroundLayer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(backgroundLayer)
        
        background2Layer.slider = self
        background2Layer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(background2Layer)
        
        trackLayer.slider = self
        trackLayer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(trackLayer)
        
        percentChar.slider = self
        percentChar.contentsScale = UIScreen.mainScreen().scale
        percentChar.opacity = 0
        
        percentChar.frame = CGRect(x: 22.0, y: 30.0, width: 30.0, height: 30.0)
        //percentChar.backgroundColor = Colors().yellow
        
        percentChar.backgroundLayer.anchorPoint = CGPointMake( 0.0, 0.5 );
        percentChar.backgroundLayer.frame = CGRect(x:0.0, y: 0.0, width: 30.0, height: 30.0)
        percentChar.backgroundLayer.bounds = CGRect(x:0.0, y: 0.0, width: 30.0, height: 30.0)
        
        percentChar.percentLayer.anchorPoint = CGPointMake( 0.0, 0.5 );
        percentChar.percentLayer.frame = CGRect(x:0.0, y: -10.0, width: 30.0, height: 30.0)
        percentChar.percentLayer.bounds = CGRect(x:0.0, y: 0.0, width: 30.0, height: 30.0)
        
        layer.addSublayer(percentChar)
        
        percentChar.setNeedsDisplay()
        
        lowerThumbLayer.slider = self
        lowerThumbLayer.textLayer.string = String(format: "%0.0f", lowerValue*100)
        lowerThumbLayer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(lowerThumbLayer)
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func update(){
        sendActionsForControlEvents(.ValueChanged)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        backgroundLayer.frame = bounds.insetBy(dx: 0.0, dy: 0.0)
        backgroundLayer.setNeedsDisplay()
        
        background2Layer.frame = bounds.insetBy(dx: 0.0, dy: 0.0)
        background2Layer.setNeedsDisplay()
        
        trackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height / 3)
        trackLayer.setNeedsDisplay()
        
        self.addTarget(self, action: "SliderValueChanged:", forControlEvents: .ValueChanged)
        
        let lowerThumbCenter = CGFloat(positionForValue(lowerValue))
        
        lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth / 2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
        lowerThumbLayer.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    func positionForValue(value: Double) -> Double {
        let widthDouble = Double(thumbWidth)
        return Double(bounds.width - thumbWidth) * (value - minimumValue) / (maximumValue - minimumValue) + Double(thumbWidth / 2.0)
    }
    
    // Touch handlers
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        previousLocation = touch.locationInView(self)
        
        // Hit test the thumb layers
        if lowerThumbLayer.frame.contains(previousLocation) {
            lowerThumbLayer.highlighted = true
            animateThumbTouch()
        }
        
        return lowerThumbLayer.highlighted
    }
    
    func boundValue(value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        let location = touch.locationInView(self)
        
        // 1. Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previousLocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - bounds.height)
        
        previousLocation = location
        
        // 2. Update the values
        if lowerThumbLayer.highlighted {
            lowerValue += deltaValue
            lowerValue = boundValue(lowerValue, toLowerValue: minimumValue, upperValue: upperValue)
        }
        
        sendActionsForControlEvents(.ValueChanged)
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
        lowerThumbLayer.highlighted = false
        animateThumbRelese()
    }
    
    
    var percHidden: Bool = true
    var animInProgress: Bool = false
    
    func animateThumbTouch(){
        UIView.animateWithDuration(1.0, delay: 0.0,
            usingSpringWithDamping: 0.1,
            initialSpringVelocity: 0.0,
            options: [],
            animations: {
                self.lowerThumbLayer.transform = CATransform3DMakeScale(1.2, 1.2, 0.0)
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func animateThumbRelese(){
        UIView.animateWithDuration(1.0, delay: 0.0,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.1,
            options: [],
            animations: {
                self.lowerThumbLayer.transform = CATransform3DMakeScale(1.0, 1.0, 0.0)
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func animatePositionIn() {
        self.percHidden = false
        
        self.percentChar.backgroundLayer.transform = CATransform3DIdentity;
        self.percentChar.percentLayer.transform = CATransform3DIdentity;
        
        self.percentChar.backgroundLayer.transform = CATransform3DMakeScale(1.0, 0.1, 1.0)
        self.percentChar.percentLayer.transform = CATransform3DMakeScale(1.0, 0.1, 1.0)
        
        UIView.animateWithDuration(1.0, delay: 0.0,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.1,
            options: [],
            animations: {
                self.percentChar.opacity = 1
                self.percentChar.backgroundLayer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
                self.percentChar.percentLayer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func animatePositionOut() {
        self.percHidden = true
        
        self.percentChar.backgroundLayer.transform = CATransform3DIdentity;
        self.percentChar.percentLayer.transform = CATransform3DIdentity;
        
        UIView.animateWithDuration(1.0,
            delay: 0.0,
            usingSpringWithDamping: 0.2,
            initialSpringVelocity: 0.1,
            options: [],
            animations: {
                self.percentChar.backgroundLayer.transform = CATransform3DMakeScale(1.0, 0.1, 1.0)
                self.percentChar.percentLayer.transform = CATransform3DMakeScale(1.0, 0.1, 1.0)
                
                self.percentChar.opacity = 0
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func animateSliderHide(time:NSTimeInterval,delay:NSTimeInterval) {
        oldPos = self.lowerThumbLayer.frame.origin.x
        trackwidth = self.trackLayer.track.frame.width
        
        //println("animateSliderHide")
        
        UIView.animateWithDuration(time,
            delay:delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                self.backgroundLayer.opacity = 0.0
                self.background2Layer.opacity = 0.0
                self.percentChar.opacity = 0.0
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func hideThumbLayer(time:NSTimeInterval,delay:NSTimeInterval){
        UIView.animateWithDuration(time,
            delay: delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                self.lowerThumbLayer.frame.origin.x = -60.0
                self.trackLayer.track.frame = CGRect(x: self.trackLayer.track.frame.origin.x, y:
                    self.trackLayer.track.frame.origin.y, width: 0.0, height: self.trackLayer.track.frame.height)
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func showThumbLayer(time:NSTimeInterval,delay:NSTimeInterval){
        UIView.animateWithDuration(time,
            delay: delay,
            options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                self.lowerThumbLayer.frame.origin.x = self.oldPos
                self.trackLayer.track.frame = CGRect(x: self.trackLayer.track.frame.origin.x, y:
                    self.trackLayer.track.frame.origin.y, width: self.trackwidth, height: self.trackLayer.track.frame.height)
            },
            completion: {_ in
                //self.appDelegate.inmotion = false
            }
        )
    }
    
    func animateSliderShow(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void) {
        UIView.animateAndChainWithDuration(time,
            delay:delay,
            options:[],
            animations: {
                self.backgroundLayer.opacity = 1.0
                self.background2Layer.opacity = 1.0
                self.percentChar.opacity = 1.0
            },
            completion:{_ in
                //self.appDelegate.inmotion = false
                callback()
            }
            ).animateWithDuration(0.7,
                animations: {
                    self.lowerThumbLayer.frame.origin.x = self.oldPos
                    self.trackLayer.track.frame = CGRect(x: self.trackLayer.track.frame.origin.x, y: self.trackLayer.track.frame.origin.y, width: CGFloat(self.positionForValue(self.lowerValue)), height: self.trackLayer.track.frame.height)
                }
        )
    }
    
    func SliderValueChanged(slider: Slider) {
        NSNotificationCenter.defaultCenter().postNotificationName("SliderValueChanged", object:nil, userInfo:["value":slider.lowerValue])
        let compareableNum = Double(floor(100*slider.lowerValue)/100);
        if compareableNum > 0.15 && percHidden == true  {
            //println("percent in")
            animatePositionIn()
        }
        if compareableNum < 0.15 && percHidden == false {
            //println("percent out")
            animatePositionOut()
        }
    }
    
}