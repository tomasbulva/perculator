import UIKit
import QuartzCore

class Calculator: UIControl {
    
    let keyboard = Keyboard()
    let topbuttons = TopButtons()
    let delKey = DelKey()
    
    var previousLocation = CGPoint()
    
//    var bgrDark:String = "#E8E1DE"
//    var bgrLight:String = "#EFE9E7"
    
    var keyboardData:[Key] = [
        Key(keyname:"1",color: true),
        Key(keyname:"2",color: false),
        Key(keyname:"3",color: true),
        
        Key(keyname:"4",color: false),
        Key(keyname:"5",color: true),
        Key(keyname:"6",color: false),
        
        Key(keyname:"7",color: true),
        Key(keyname:"8",color: false),
        Key(keyname:"9",color: true),
        
        Key(keyname:"\u{232B}",color: false),
        Key(keyname:"0",color: true),
        Key(keyname:".",color: false)
    ]
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        keyboard.calculator = self
        keyboard.contentsScale = UIScreen.mainScreen().scale
        keyboard.opacity = 0
        layer.addSublayer(keyboard)
        
        topbuttons.calculator = self
        topbuttons.opacity = 0
        layer.addSublayer(topbuttons)
        
        delKey.calculator = self
        delKey.opacity = 0
        layer.addSublayer(delKey)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        
        let topHalfOfScreen = CGRect(x:0,y:0,width:screenWidth,height:screenHeight/2)
        let bottomHalfOfScreen = CGRect(x:0,y:screenHeight/2,width:screenWidth,height:screenHeight/2)
        
        keyboard.frame = bottomHalfOfScreen
        keyboard.setNeedsDisplay()
        
        topbuttons.frame = topHalfOfScreen
        topbuttons.setNeedsDisplay()
        
        delKey.frame = CGRect(x:screenWidth*0.13,y:screenHeight-screenHeight*0.08,width:160.0,height:160.0)
        delKey.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        //println("===== test =====")
        var location = touch.locationInView(self)
        var sublocation = keyboard.superlayer!.convertPoint(location, toLayer:keyboard)
        
        var fingerRect = CGRectMake(sublocation.x-5, sublocation.y-5, 10, 10);
        
        // Hit test the numbers
        var i = 0
        for pos in self.keyboard.keyBgr {
            let key = self.keyboard.keyBgr[i]
            
            var frame = key.frame
            
            if CGRectIntersectsRect(fingerRect,frame) {
            
                //println("hit \(self.keyboard.keyLabel[i].string)")
            
                NSNotificationCenter.defaultCenter().postNotificationName("keyboardHit", object:nil, userInfo:["key":self.keyboard.keyLabel[i].string,"keyIndex":i])
            }
            i++
        }
        
        var sublocation2 = topbuttons.superlayer!.convertPoint(location, toLayer:topbuttons)
        
        var fingerRect2 = CGRectMake(sublocation2.x-5, sublocation2.y-5, 10, 10);

        if CGRectIntersectsRect(fingerRect2,topbuttons.cancelButton.frame) {
            //println("hit cancel")
            NSNotificationCenter.defaultCenter().postNotificationName("keyboardViewButtons", object:nil, userInfo:["key":"cancel"])
        }
        if CGRectIntersectsRect(fingerRect2,topbuttons.percalculateButton.frame){
            //println("hit perculate")
            NSNotificationCenter.defaultCenter().postNotificationName("keyboardViewButtons", object:nil, userInfo:["key":"perculate"])
        }
        
        return true
    }
    
    func animateCalcHide(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void){
        UIView.animateWithDuration(0.75,
            delay: 0.5,
            options: [], //UIViewAnimationOptions.CurveEaseOut
            animations: {
                self.topbuttons.opacity = 0
                self.keyboard.opacity = 0
                self.delKey.opacity = 0
            },
            completion: { _ in
                //println("calc hide")
                callback()
        })
        NSTimer.scheduledTimerWithTimeInterval(1.3, target:self, selector: Selector("removeCalc"), userInfo: nil, repeats: false)
    }
    
    func removeCalc(){
        self.removeFromSuperview()
    }
    
    func animateCalcShow(time:NSTimeInterval,delay:NSTimeInterval,callback:()->Void) {
        //println("self.topbuttons.opacity \(self.topbuttons.opacity)")
        //println("self.keyboard.opacity \(self.keyboard.opacity)")
        //println("self.delKey.opacity \(self.delKey.opacity)")
        UIView.animateWithDuration(time,
            delay: delay,
            options: [], //UIViewAnimationOptions.CurveEaseOut
            animations: {
                self.topbuttons.opacity = 1
                self.keyboard.opacity = 1
                self.delKey.opacity = 1
            },
            completion: {_ in
                callback()
            }
        )
    }
}