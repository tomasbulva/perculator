import UIKit
import QuartzCore

class Graphics: CALayer {
    
    weak var onboarding: OnBoarding?

    let top = CATextLayer()
    let topstring = "tap to change original amount"
    
    let middle = CATextLayer()
    let middlestring = "slide to adjust"
    
    let middle2 = CATextLayer()
    let middle2string = "percentage"
    
    let bottom = CATextLayer()
    let bottomstring = "tap to change percentage \ncalculation"
    
    let close = CATextLayer()
    let version = CATextLayer()
    
    let background = CAShapeLayer()
    let topCircle = CAShapeLayer()
    let middleCircle = CAShapeLayer()
    let bottomCircle = CAShapeLayer()
    
    let rightTrack = CAShapeLayer()
    let littleTrackCircle = CAShapeLayer()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //let screenHeight = UIScreen.mainScreen().bounds.size.height
    
    override func drawInContext(ctx: CGContext) {
        if let onboarding = onboarding {
            setUpLayer()
        }
    }
    
    func setUpLayer() {
        
        //println("Graphics setUpLayer")
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        
        //smallIphone = screenHeight <= 960 ? true : false
        let topCircleMargin = screenHeight <= 480 ? bounds.height*0.08 : bounds.height*0.04
        let bottomCircleMargin = screenHeight <= 480 ? bounds.height*0.08 : bounds.height*0.03
        
        //println("screenHeight \(screenHeight)")
        //println("topCircleMargin \(topCircleMargin)")
        
        
        background.frame = CGRect(x:0.0,y:0.0,width:screenWidth,height:screenHeight)
        background.backgroundColor = Colors().darkBlue
        background.opacity = 0.5
        
        addSublayer(background)
        
        
        
        let radius: CGFloat = bounds.height/3
        
        topCircle.path = UIBezierPath(roundedRect: CGRect(x: CGRectGetMidX(self.frame) - (radius/2), y: CGRectGetMidX(self.frame) - (radius/2)-topCircleMargin, width: radius, height: radius), cornerRadius: radius).CGPath
        
        topCircle.lineCap = kCALineCapButt
        topCircle.lineDashPattern = [4,4]
        topCircle.lineDashPhase = 0.5
        topCircle.lineJoin = kCALineJoinMiter
        topCircle.lineWidth = 2.0
        topCircle.miterLimit = 0.0
        topCircle.strokeColor = Colors().white
        topCircle.fillColor = Colors().noColor
        
        addSublayer(topCircle)
        
        
        
        bottomCircle.path = UIBezierPath(roundedRect: CGRect(x: CGRectGetMidX(self.frame) - (radius/2), y: CGRectGetMidX(self.frame) - (radius/2)-bottomCircleMargin + (bounds.height/2), width: radius, height: radius), cornerRadius: radius).CGPath
        
        bottomCircle.lineCap = kCALineCapButt
        bottomCircle.lineDashPattern = [4,4]
        bottomCircle.lineDashPhase = 0.5
        bottomCircle.lineJoin = kCALineJoinMiter
        bottomCircle.lineWidth = 2.0
        bottomCircle.miterLimit = 0.0
        bottomCircle.strokeColor = Colors().white
        bottomCircle.fillColor = Colors().noColor
        
        addSublayer(bottomCircle)
        
        
        
        let radius2: CGFloat = bounds.height/9
        
        middleCircle.path = UIBezierPath(roundedRect: CGRect(x: CGRectGetMidX(self.frame) - (radius2/2), y: CGRectGetMidY(self.frame) - (radius2/2), width: radius2, height: radius2), cornerRadius: radius2).CGPath
        
        middleCircle.lineCap = kCALineCapButt
        middleCircle.lineDashPattern = [4,4]
        middleCircle.lineDashPhase = 0.5
        middleCircle.lineJoin = kCALineJoinMiter
        middleCircle.lineWidth = 2.0
        middleCircle.miterLimit = 0.0
        middleCircle.strokeColor = Colors().white
        middleCircle.fillColor = Colors().noColor
        
        addSublayer(middleCircle)
        
        
        
        let radius3: CGFloat = 10
        
        littleTrackCircle.path = UIBezierPath(roundedRect: CGRect(x: bounds.width-37, y: CGRectGetMidY(self.frame) - (radius3/2), width: radius3, height: radius3), cornerRadius: radius3).CGPath
        
        littleTrackCircle.lineCap = kCALineCapButt
        littleTrackCircle.lineJoin = kCALineJoinMiter
        littleTrackCircle.lineWidth = 2.0
        littleTrackCircle.miterLimit = 0.0
        littleTrackCircle.strokeColor = Colors().white
        littleTrackCircle.fillColor = Colors().noColor
        
        addSublayer(littleTrackCircle)
        
        
        
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x:CGRectGetMidX(self.frame)+(radius2/2), y:CGRectGetMidY(self.frame)))
        path.addLineToPoint(CGPoint(x:bounds.width-40, y:CGRectGetMidY(self.frame)))
        
        //design path in layer
        rightTrack.path = path.CGPath
        rightTrack.strokeColor = Colors().white
        rightTrack.lineWidth = 2.0
        rightTrack.lineDashPattern = [4,4]
        rightTrack.lineCap = kCALineCapButt
        
        addSublayer(rightTrack)
        
        
        
        top.frame = CGRect(x:CGRectGetMidX(self.frame)-(radius/4),y:CGRectGetMidX(self.frame) - (radius/2)-topCircleMargin+10,width:radius/2,height:40.0)
        top.foregroundColor = Colors().white
        top.opacity = 1
        top.wrapped = true
        top.fontSize = 12
        top.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        top.alignmentMode = kCAAlignmentCenter
        top.contentsScale = UIScreen.mainScreen().scale
        top.string = topstring
        
        middle.frame = CGRect(x:(bounds.width/2)+(radius2/2)+5,y:bounds.height/2-25,width:bounds.width,height:20.0)
        middle.foregroundColor = Colors().white
        middle.opacity = 1
        middle.fontSize = 15
        middle.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        middle.alignmentMode = kCAAlignmentLeft
        middle.contentsScale = UIScreen.mainScreen().scale
        middle.string = middlestring
        
        middle2.frame = CGRect(x:(bounds.width/2)+(radius2/2)+5,y:bounds.height/2+2,width:bounds.width,height:20.0)
        middle2.foregroundColor = Colors().white
        middle2.opacity = 1
        middle2.fontSize = 15
        middle2.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        middle2.alignmentMode = kCAAlignmentLeft
        middle2.contentsScale = UIScreen.mainScreen().scale
        middle2.string = middle2string
        
        //println("bottomCircle.bounds.width \(CGRectGetMidX(self.frame) - (radius/2)-bottomCircleMargin + (bounds.height/2))")
        
        bottom.frame = CGRect(x:CGRectGetMidX(self.frame)-(radius/2.6),y:CGRectGetMidX(self.frame) - (radius/2)-bottomCircleMargin + (bounds.height/2)+radius-50,width:radius/1.3,height:40.0)
        bottom.foregroundColor = Colors().white
        bottom.opacity = 1
        bottom.wrapped = true
        bottom.fontSize = 12
        bottom.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        bottom.alignmentMode = kCAAlignmentCenter
        bottom.contentsScale = UIScreen.mainScreen().scale
        bottom.string = bottomstring
        
        close.frame = CGRect(x: self.bounds.width-45,y:self.bounds.height-50,width:30,height:30.0)
        close.foregroundColor = Colors().white
        close.opacity = 1
        close.fontSize = 30
        close.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        close.alignmentMode = kCAAlignmentCenter
        close.contentsScale = UIScreen.mainScreen().scale
        close.string = "\u{00D7}"
        
        version.frame = CGRect(x: 25,y:self.bounds.height-35,width:200,height:30.0)
        version.foregroundColor = Colors().white
        version.opacity = 1
        version.fontSize = 10
        version.font = CTFontCreateWithName("AvenirNext-Regular", top.fontSize, nil)
        version.alignmentMode = kCAAlignmentLeft
        version.contentsScale = UIScreen.mainScreen().scale
        version.string = appDelegate.versionString
        
        addSublayer(top)
        addSublayer(middle)
        addSublayer(middle2)
        addSublayer(bottom)
        addSublayer(close)
        addSublayer(version)
        
    }

    
}