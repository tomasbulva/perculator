import UIKit
import QuartzCore

class SliderTrackPatternLayer: CALayer {
    weak var slider: Slider?
    
    override func drawInContext(ctx: CGContext) {
        if let slider = slider {
            
            //let cornerRadius = bounds.height * slider.curvaceousness / 2.0
            let path = UIBezierPath(CGPath: CGPathCreateMutable()) //(roundedRect: bounds, cornerRadius: 0.0)
            
            CGContextAddPath(ctx, path.CGPath)
            CGContextSetLineDash(ctx,0,[1,1],2)
            CGContextSetStrokeColorWithColor(ctx, Colors().red);
            CGContextSetLineWidth(ctx, 1.0);

            let verticalCenter = bounds.height / 2.0
            CGContextMoveToPoint(ctx, 0.0, verticalCenter);
            
            CGContextAddLineToPoint(ctx, bounds.width, verticalCenter);
            
            //Draw it
            CGContextStrokePath(ctx);
            
        }
    }
}